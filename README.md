# QA Automation Demo

Made with Java using the [Selenide](https://selenide.org/) framework

## Description

###### GoogleTestFirst.java

The script open the browser, checks the search input, search button and I'm feeling Lucky button to exist

###### GoogleTestSecond.java

The script opens a browser, searches for a term and checks the second search result

###### GoogleTestThird.java

The script opens a browser, writes something in the search input, checks the suggestion box has appeared then checks all the suggestions and makes sure the initial term is present

###### GoogleTestFourth.java

The script searches for a term, clicks on the Next button, checks the second page of results has been loaded, clicks on Previous and checks if the first page of results has been loaded

###### GoogleTestFifth.java

The script searches for a term, checks the search input is present on the page, scrolls down and checks the search input to still be visible